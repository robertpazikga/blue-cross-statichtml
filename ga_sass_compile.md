1. Install Node.js
  https://nodejs.org/en/download/

2. In terminal:
  Go to project directory.

  `cd docroot/themes/custom/climate_week/ && npm install`

3. Wait till the node finished downloading files.
7. To start sass watcher type command `npm run watch-css` in your terminal window. Watcher will automatically.
