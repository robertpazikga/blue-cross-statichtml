(function($) {
  // setTimeout(function(){
  //   alert('this is test');
  // },1000);
var stateTogglers = {
  tabs: function() {

  },
  amounts: function() {
    var input = $('.amount-picker input');
    input.each(function() {
      if ($(this).attr('type') == 'radio') {
        $(this).on('change', function() {
          $('.amount-picker li').removeClass('active');
            $(this).parent().parent().addClass('active');
            if ($(this).val() )
            var infoText = $(this).next().html();
            $('.amount-info-text').empty();
            $('.amount-info-text').append(infoText);
        });
      }
    })
  },
  inputs: function() {
    $('input, textarea').on('keyup',function(e) {
      if ($(this).attr('type') !== 'checkbox' || $(this).attr('type') !== 'radio') {
        if ($(this).val().length > 0) {
          $(this).parent().addClass('added');
        } else {
          $(this).parent().removeClass('added');
        }
      }
    });
    $('input').each(function(){
      if ($(this).attr('type') == 'checkbox' && $(this).next('input').attr('type') == 'number') {
        $(this).on('click', function() {
          $(this).next().focus();
        });
      }
      if ($(this).attr('type') == 'number' && $(this).prev('input').attr('type') == 'checkbox') {
        $(this).on('focus', function() {
          $(this).next().attr('checked','checked');
        });
      }
    });
  },
  inputLabel: function() {
    $('input, textarea').on('focus', function() {
      if ($(this).attr('type') !== 'checkbox' || $(this).attr('type') !== 'radio') {
        //$(this).prev().trigger('click');
        $(this).parent().addClass('focused');
        $(this).blur(function() {
          $(this).parent().removeClass('focused');
        });
      }
    });
    $('.input-checkbox input, .input-radio input').on('change', function() {
      //$(this).prev().trigger('click');
      if ($(this).is(':checked')) {
        // //cosole.log('is checked');
        $(this).parent().addClass('checked');
      } else {
        $(this).parent().removeClass('checked');
      }
    });
    $('label').on('click',function(e) {

      $(this).next().focus(function() {
        $(this).parent().addClass('focused');
        $(this).blur(function() {
          $(this).parent().removeClass('focused');
        });
      });
      // $(this).next().trigger('focus');
      // $(this).parent().addClass('focused');
      // if ($(this).next().val().length > 0) {
      //   $(this).parent().removeClass('focused');
      // }
    });
  },
  amoutTextFocus: function() {

  },
  inputHelpers: function() {
    var helperButton = $('.input-helper button');
    if (helperButton.length > 0) {
      helperButton.each(function() {

        var string_text = $(this).attr('tooltip-data');
        if (string_text != '') {
          var html_code = '<div class="tooltip-data-show">'+string_text+'</div>'
          $(this).parent().append(html_code);
          $(window).trigger('resize');

          // $('.help-info').on('click tap', function() {
          //   $(this).next().toggleClass('active');
          //   // if($(this).next('.tooltip-data-show').hasClass('active') != true) {
          //   //   $(this).next().addClass('active');
          //   // } else {
          //   //   $(this).next().removeClass('active');
          //   // }
          // });
        }
      });
    }
  },
  multiline: function(text) {
    this.text(text);
    this.html(this.html().replace(/\n/g,'<br/>'));
    return this;
  },
  giftAidToggle: function() {
    $('.giftaid-section input:checkbox').change(function(){
      if (this.checked == true) {
        $('.giftaid-info').addClass('active');
      } else {
        $('.giftaid-info').removeClass('active');
      }
  });
  },
  hiddenForms: function() {
    $('.reveal-address').on('click',function() {
      $('.input-hidden-manual').removeClass('input-hidden-manual');
    });
  },
  textareaEmail: function() {
    if ($('.email-message-area textarea').length > 0) {
      var text_value;
      $('.email-message-area textarea').bind('input propertychange', function() {
        ////cosole.log(this.value);
        text_value = this.value;
        $('.email-rendered-display .text-content #contents').html(text_value.replace(/\r?\n/g,'<br/>'));
      });
    }
  },
  watchInputs:function () {
    $('input').each(function() {
      if ($(this).attr("required")) {
        var input_type = $(this).attr('type');
        $(this).blur(function(event) {
          var _this = $(this);
          var event_type = event.type;
          stateTogglers.formValidation(_this,input_type,event_type);
        });
        $(this).on('keyup',function(event) {
          var event_type = event.type;
          var _this = $(this);
          stateTogglers.formValidation(_this,input_type,event_type);
        });
      }
    });
  },
  fieldAlert:function(_this,error_type){
    if (error_type == 'error') {
      $(_this).parent().addClass('input-validation-error');
    }
    if (error_type == 'success') {
      $(_this).parent().removeClass('input-validation-error');
    }
  },
  formValidation:function(_this, input, event) {
    switch(input) {
        case 'text':
          if(event == 'blur') {
            if ($(_this).val().length < 1) {
              stateTogglers.fieldAlert(_this,'error');
            }
          }
          if (event == 'keyup') {
            if ($(_this).val().length > 0) {
              stateTogglers.fieldAlert(_this,'success');
            }
          }
          break;
        case 'email':
            if(event == 'blur') {
              if ($(_this).val().length > 0) {
                var field_email = $(_this).val();
                if(/(.+)@(.+){2,}\.(.+){2,}/.test(field_email) ){
                  //
                } else {
                  //cosole.log('email not matches');
                  stateTogglers.fieldAlert(_this,'error');
                }
              } else {
                stateTogglers.fieldAlert(_this,'error');
              }
            }
            if (event == 'keyup') {
              if ($(_this).val().length > 0) {
                var field_email = $(_this).val();
                if(/(.+)@(.+){2,}\.(.+){2,}/.test(field_email) ){
                  //cosole.log('email matches');
                  stateTogglers.fieldAlert(_this,'success');
                }
              }
            }
          break;
        default:
          if(event == 'blur') {
            if ($(_this).val().length < 1) {
              stateTogglers.fieldAlert(_this,'error');
            }
          }
          if (event == 'keyup') {
            if ($(_this).val().length > 0) {
              stateTogglers.fieldAlert(_this,'success');
            }
          }
          break;
    }
    //cosole.log(_this);
    //cosole.log($(_this).val());
    //cosole.log(input);
    //cosole.log(event);
  }
}

$(document).ready(stateTogglers.amounts());
$(document).ready(stateTogglers.inputs());
$(document).ready(stateTogglers.inputLabel());
$(document).ready(stateTogglers.inputHelpers());
$(document).ready(stateTogglers.hiddenForms());
$(document).ready(stateTogglers.textareaEmail());
$(document).ready(stateTogglers.watchInputs());
$(document).ready(stateTogglers.giftAidToggle());

$(window).ready('popstate', function() {
  $('input').each(function() {
    if ($(this).val().length > 0) {
      $(this).addClass('added');
      $(this).parent().addClass('added');
    }
  });
});

})(jQuery);
